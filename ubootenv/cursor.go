// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ubootenv

import (
	"bytes"
	"errors"
)

var (
	// ErrInsufficientSpace records inability to perform modifications due to insufficient space.
	ErrInsufficientSpace = errors.New("insufficient remaining space")

	// ErrCorruptedEntry records corrupted entry devoid of the '=' byte.
	ErrCorruptedEntry = errors.New("corrupted entry")

	// ErrInvalidCursor records attempt to use the cursor at an invalid position.
	ErrInvalidCursor = errors.New("cursor is not pointing to a valid entry")
)

// Cursor navigates entries in an U-Boot environment block.
//
// At any time the cursor is either at a valid position corresponding to an
// entry or is at the end of the block.
//
// Subsequent calls of Next advance cursor position.
type Cursor struct {
	buf []byte

	// The start and end fields designate a span in buf representing the entry
	// at the cursor position. The end value is the last byte of the entry (NUL).
	start int
	end   int
}

// NewCursor returns a cursor for the given buffer.
//
// The cursor may modify the buffer if any of the mutating functions, like
// SetValue, Delete and Insert, are called.
func NewCursor(buf []byte) *Cursor {
	return &Cursor{buf: buf}
}

// Next advances towards the next key-value entry.
func (cur *Cursor) Next() bool {
	// Advance to the next position.
	cur.start = cur.end

	// Ensuring we are not exceeding buffer size, look for the next record.
	for cur.start < len(cur.buf) {
		var end int

		// Scan the buffer while looking for the record terminator byte (NUL).
		for end = cur.start; end < len(cur.buf) && cur.buf[end] != 0; end++ {
		}

		if end < len(cur.buf) {
			// If end does not point beyond the buffer then the entry is
			// normally terminated with a NUL byte. We want to include the
			// record terminator in the sliced view so advance end one more
			// time.
			end++
		}

		cur.end = end

		// If the record starts with the record terminator then we've arrived
		// at the unused space.
		if cur.start < len(cur.buf) && cur.buf[cur.start] == 0 {
			// Avdance over this record and let the outer loop deplete.
			cur.start = cur.end
			continue
		}

		// We've found a record.
		return true
	}

	return false
}

// Entry returns the entry at the current cursor position.
//
// The entry is returned in its raw form, it cannot be used directly.
func (cur *Cursor) Entry() string {
	if bufLen := len(cur.buf); cur.start < bufLen && cur.end <= bufLen {
		return string(cur.buf[cur.start:cur.end])
	}

	return ""
}

// Key returns the name of the entry at the cursor position.
func (cur *Cursor) Key() string {
	if bufLen := len(cur.buf); cur.start < bufLen && cur.end <= bufLen {
		span := cur.buf[cur.start:cur.end]
		if off := bytes.IndexByte(span, '='); off != -1 {
			return string(span[:off])
		}
	}

	return ""
}

// Value returns the value of the entry at the cursor position.
func (cur *Cursor) Value() string {
	if bufLen := len(cur.buf); cur.start < bufLen && cur.end <= bufLen {
		var span []byte

		if cur.end > 0 && cur.buf[cur.end-1] == 0 {
			span = cur.buf[cur.start : cur.end-1]
		} else {
			span = cur.buf[cur.start:cur.end]
		}

		if off := bytes.IndexByte(span, '='); off != -1 {
			return string(span[off+1:])
		}
	}

	return ""
}

// SetValue replaces the value of the current key.
//
// Depending on the size of the entry at the cursor, the new value either:
// replaces the old value in place, uses less space than before, causing all the
// remaining records to shift left, uses more space than before, causing all the
// remaining records to shift right if sufficient space is available.
//
// SetValue fails if the current entry is corrupted or if storing the new value
// would exceed available space.
func (cur *Cursor) SetValue(value string) error {
	if bufLen := len(cur.buf); cur.start < bufLen && cur.end <= bufLen {
		span := cur.buf[cur.start:cur.end]

		off := bytes.IndexByte(span, '=')
		if off == -1 {
			return ErrCorruptedEntry
		}

		wantedCap := off + len(value) + 2
		buf := bytes.NewBuffer(make([]byte, 0, wantedCap))

		// Terminate the value to form a valid entry.
		_, _ = buf.Write(span[:off+1])
		_, _ = buf.Write([]byte(value))
		_ = buf.WriteByte(0)

		return cur.setEntry(buf.Bytes())
	}

	return ErrInvalidCursor
}

// Delete deletes the current entry.
//
// Immediately after calling Delete, the current entry
// is invalid. Call Next to find the next valid entry.
func (cur *Cursor) Delete() {
	if err := cur.resizeEntryBy(-(cur.end - cur.start)); err != nil {
		panic(err)
	}

	// Reset the span designated by the current entry.
	cur.end = cur.start
}

// InsertBefore creates a new entry at the current cursor position.
//
// The cursor should not be at the initial header position as doing so will
// create an incorrect environment file by shifting the header down the buffer.
func (cur *Cursor) InsertBefore(key, value string) error {
	wantedCap := len(key) + len(value) + 2
	entryBuf := bytes.NewBuffer(make([]byte, 0, wantedCap))

	// Escape and terminate the value to form a valid entry.
	_, _ = entryBuf.WriteString(key)
	_ = entryBuf.WriteByte('=')
	_, _ = entryBuf.Write([]byte(value))
	_ = entryBuf.WriteByte(0)
	entry := entryBuf.Bytes()

	delta := len(entry)

	if bufLen := len(cur.buf); cur.start < bufLen && cur.end <= bufLen {
		// If we are in the middle of the buffer, resize the current entry and copy it to the right.
		if err := cur.resizeEntryBy(delta); err != nil {
			return err
		}

		// Move the current entry to the right.
		copy(cur.buf[cur.end:], cur.buf[cur.start:cur.end])
	} else if cur.start == bufLen {
		// If we are at the end of the buffer than see if we can fit into the unused space.
		free := cur.freeSpace()
		if delta > free {
			return ErrInsufficientSpace
		}

		// Move the cursor to the first unused byte.
		cur.start = len(cur.buf) - free
	}

	// Copy the new entry.
	copy(cur.buf[cur.start:cur.start+len(entry)], entry)

	// Update the span of the current entry.
	cur.end = cur.start + len(entry)

	return nil
}

// setEntry sets the entry at the current cursor position.
//
// Old entry at the cursor is overwritten and replaced. Depending on the size of
// the current and new entry, the data after the current entry is shifted in
// left or right.
func (cur *Cursor) setEntry(entry []byte) error {
	if bufLen := len(cur.buf); cur.start < bufLen && cur.end <= bufLen {
		// Delta is the difference in length of the new value.
		// Values smaller than zero imply shorter new value.
		delta := len(entry) - (cur.end - cur.start)

		if err := cur.resizeEntryBy(delta); err != nil {
			return err
		}

		// Copy the new entry.
		copy(cur.buf[cur.start:], entry)

		// Remember the size of the current entry.
		cur.end += delta

		return nil
	}

	return ErrInvalidCursor
}

func (cur *Cursor) resizeEntryBy(delta int) error {
	switch {
	case delta < 0: // Shift data left.
		// Shift the remaining entries. Note that delta is negative so we are
		// overwriting. The last "delta" bytes with what follows after the
		// current record, effectively erasing those bytes.
		copy(cur.buf[cur.end+delta:], cur.buf[cur.end:])

		// Mark space at the far-right as unused.
		for i := -delta; i > 0; i-- {
			cur.buf[len(cur.buf)-i] = 0
		}

	case delta == 0: // There is nothing to do.

	case delta > 0: // Shift data right.
		// Check if we have sufficient space.
		if delta > cur.freeSpace() {
			return ErrInsufficientSpace
		}

		// Shift the remaining entries right to make space.
		copy(cur.buf[cur.end+delta:], cur.buf[cur.end:])
	}

	return nil
}

// freeSpace returns the number of unused bytes of memory at the end of the block.
func (cur *Cursor) freeSpace() int {
	var free int

	for i := len(cur.buf) - 1; i >= 0 && cur.buf[i] == 0; i-- {
		free++
	}

	// The last NUL byte is the terminator of the final entry, unless the
	// entire buffer is empty.
	if cur.buf[0] != 0 && free > 0 {
		free--
	}

	return free
}
