// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ubootenv_test

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"testing"

	"gitlab.com/zyga-aka-zygoon/go-u-boot/ubootenv"
)

func TestDeviceMarshaling(t *testing.T) {
	for i, tc := range []struct {
		txt          string          // textual representation to unmarshal
		canonicalTxt string          // canonical representation, if different from txt
		d            ubootenv.Device // device if txt can be unmarshaled
		errMsg       string          // error if txt cannot be unmarshaled
	}{
		{
			txt: "/dev/mtd0 0x1000 0x2000",
			d:   ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt:          "/dev/mtd0 0x1000 0x2000 0",
			canonicalTxt: "/dev/mtd0 0x1000 0x2000",
			d:            ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt:          "/dev/mtd0 0x1000 0x2000 0x2000",
			canonicalTxt: "/dev/mtd0 0x1000 0x2000",
			d:            ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},

		{
			txt:          "/dev/mtd0 0x1000 0x2000 0 0",
			canonicalTxt: "/dev/mtd0 0x1000 0x2000",
			d:            ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt:          "/dev/mtd0 0x1000 0x2000 0 1",
			canonicalTxt: "/dev/mtd0 0x1000 0x2000",
			d:            ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt:          "/dev/mtd0 0x1000 0x2000 0x2000 1",
			canonicalTxt: "/dev/mtd0 0x1000 0x2000",
			d:            ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt: "/dev/mtd0 0x1000 0x2000 0x1000 2",
			d:   ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x1000, SectorCount: 2},
		},
		{
			txt: "/dev/mtd0 -0x1000 0x2000",
			d:   ubootenv.Device{DeviceName: "/dev/mtd0", DeviceOffset: -0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt:    "/dev/mtd0 0x1000 0x2000 0 potato",
			errMsg: `strconv.ParseInt: parsing "potato": invalid syntax`,
		},
		{
			txt:    "/dev/mtd0 0x1000 0x2000 potato 0",
			errMsg: `strconv.ParseInt: parsing "potato": invalid syntax`,
		},
		{
			txt:    "/dev/mtd0 0x1000 potato",
			errMsg: `strconv.ParseInt: parsing "potato": invalid syntax`,
		},
		{
			txt:    "/dev/mtd0 potato 0x2000",
			errMsg: `strconv.ParseInt: parsing "potato": invalid syntax`,
		},
		{
			txt: "potato 0x1000 0x2000",
			d:   ubootenv.Device{DeviceName: "potato", DeviceOffset: 0x1000, EnvSize: 0x2000, SectorSize: 0x2000, SectorCount: 1},
		},
		{
			txt:    "/dev/mtd0 -0x1000 0x2000 -1",
			errMsg: "sector size cannot be negative",
		},
		{
			txt:    "/dev/mtd0 -0x1000 0x2000 0 -1",
			errMsg: "sector count cannot be negative",
		},
		{
			txt:    "/dev/mtd0 -0x1000 0 0 0",
			errMsg: "environment size must be greater than zero",
		},
		{
			txt:    "/dev/mtd0 -0x1000 -0x2000 0 0",
			errMsg: "environment size must be greater than zero",
		},
		{
			txt:    "/dev/mtd0 0x1000 0x2000 0 0 potato",
			errMsg: "expected device name, offset and environment size, sector size and sector count",
		},
		{
			txt:    "/dev/mtd0 0x1000",
			errMsg: "expected device name, offset and environment size",
		},
		{
			txt:    "/dev/mtd0",
			errMsg: "expected device name, offset and environment size",
		},
		{
			txt:    "",
			errMsg: "expected device name, offset and environment size",
		},
	} {
		t.Run(strconv.Itoa(i), func(t *testing.T) {
			var d ubootenv.Device

			err := d.UnmarshalText([]byte(tc.txt))

			if tc.errMsg == "" {
				if err != nil {
					t.Fatalf("Cannot unmarshal %q: %v", tc.txt, err)
				}

				if !reflect.DeepEqual(d, tc.d) {
					t.Fatalf("Unmarshaled device differs: expected: %#v, got %#v", tc.d, d)
				}

				b, err := d.MarshalText()
				if err != nil {
					t.Fatalf("Cannot marhsal %#v: %v", d, err)
				}

				wantedB := []byte(tc.canonicalTxt)
				if len(wantedB) == 0 {
					wantedB = []byte(tc.txt)
				}

				if !bytes.Equal(b, wantedB) {
					t.Fatalf("Incorrect round trip %q != %q", string(b), wantedB)
				}
			} else {
				if err == nil {
					t.Fatalf("Unexpected success to unmarshal %q", tc.txt)
				}

				if err.Error() != tc.errMsg {
					t.Fatalf("Unexpected error: %q, expected %q", err, tc.errMsg)
				}
			}
		})
	}
}

func TestConfigFileLoad(t *testing.T) {
	t.Run("working", func(t *testing.T) {
		cf := ubootenv.ConfigFile("testdata/fw_env.config")

		cfg, err := cf.LoadConfig()
		if err != nil {
			t.Fatal(err)
		}

		expected := &ubootenv.Config{Device: ubootenv.Device{
			DeviceName: "/boot/uboot.env", EnvSize: 0x4000, SectorSize: 0x4000, SectorCount: 1,
		}}

		if !reflect.DeepEqual(cfg, expected) {
			t.Fatalf("Loaded config differs: expected: %#v, got %#v", expected, cfg)
		}
	})

	t.Run("missing", func(t *testing.T) {
		cf := ubootenv.ConfigFile("testdata/missing.config")

		_, err := cf.LoadConfig()
		if !errors.Is(err, os.ErrNotExist) {
			t.Fatalf("unexpected error: %v", err)
		}
	})
}

func TestLoadConfig(t *testing.T) {
	cfg, err := ubootenv.LoadConfig("testdata/fw_env.config")
	if err != nil {
		t.Fatalf("Cannot load config: %s", err)
	}

	expected := &ubootenv.Config{Device: ubootenv.Device{
		DeviceName: "/boot/uboot.env", EnvSize: 0x4000, SectorSize: 0x4000, SectorCount: 1,
	}}
	if !reflect.DeepEqual(cfg, expected) {
		t.Fatalf("Loaded config differs: expected: %#v, got %#v", expected, cfg)
	}

	_, err = ubootenv.LoadConfig("testdata/missing.config")
	if err == nil {
		t.Fatalf("Unexpected success to load missing config file")
	}

	if !errors.Is(err, os.ErrNotExist) {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func TestReadConfig(t *testing.T) {
	const s = `
# Comments are ignored
# Lots of comments

# Empty lines are also ignored
/dev/mtd0 0 0x1000
/dev/mtd0 -0x1000 0x1000
`

	cfg, err := ubootenv.ReadConfig(strings.NewReader(s), "fw_env.config")
	if err != nil {
		t.Fatalf("Cannot read config: %s", err)
	}

	expected := &ubootenv.Config{
		Device: ubootenv.Device{
			DeviceName:  "/dev/mtd0",
			EnvSize:     0x1000,
			SectorSize:  0x1000,
			SectorCount: 1,
		},
		RedundantDevice: &ubootenv.Device{
			DeviceName:   "/dev/mtd0",
			DeviceOffset: -0x1000,
			EnvSize:      0x1000,
			SectorSize:   0x1000,
			SectorCount:  1,
		},
	}
	if !reflect.DeepEqual(cfg, expected) {
		t.Fatalf("Loaded config differs: expected: %#v, got %#v", expected, cfg)
	}

	// Primary and redundant device have different environment size.
	const sizeMismatch = `
/dev/mtd0 0 0x1000
/dev/mtd0 -0x1000 0x4000
`

	_, err = ubootenv.ReadConfig(strings.NewReader(sizeMismatch), "fw_env.config")
	if err == nil {
		t.Fatalf("Unexpected success to read faulty config")
	}

	if err.Error() != "fw_env.config:3: primary and redundant device must use the same environment size" {
		t.Fatalf("Unexpected error: %v", err)
	}

	// Insufficient number of devices
	const noDevs = ``

	_, err = ubootenv.ReadConfig(strings.NewReader(noDevs), "fw_env.config")
	if err == nil {
		t.Fatalf("Unexpected success to read faulty config")
	}

	if err.Error() != "fw_env.config:1: at least one device is required" {
		t.Fatalf("Unexpected error: %v", err)
	}

	err = errors.Unwrap(err)
	if err == nil {
		t.Fatalf("Expected to be able to unwrap the error")
	}

	if err.Error() != "at least one device is required" {
		t.Fatalf("Unexpected error: %v", err)
	}

	// Too many devices
	const threeDevs = `
/dev/mtd0 0 0x1000
/dev/mtd0 0x1000 0x1000
/dev/mtd0 0x2000 0x1000
`

	_, err = ubootenv.ReadConfig(strings.NewReader(threeDevs), "fw_env.config")
	if err == nil {
		t.Fatalf("Unexpected success to read faulty config")
	}

	if err.Error() != "fw_env.config:4: at most two devices are allowed" {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func TestDeviceLoadData(t *testing.T) {
	for _, tc := range []struct {
		size, offset int64
	}{
		{0x4000, 0},       // entire file is env data
		{0x8000, 0},       // first half of the file is env data
		{0x8000, -0x4000}, // second half of the file is env data
		{0x8000, 0x4000},  // same as above
		{0x8000, 0x2000},  // env data is in the middle
		{0x8000, -0x6000}, // same as above
	} {
		t.Run(fmt.Sprintf("size:%#x,offset:%#x", tc.size, tc.offset), func(t *testing.T) {
			d := ubootenv.Device{
				DeviceName:   ubootEnvAtOffset(t, tc.size, tc.offset),
				DeviceOffset: int(tc.offset), // TODO(zyga): change the interface to use int64.
				EnvSize:      0x4000,
			}

			data, err := d.LoadData()
			if err != nil {
				t.Fatalf("Cannot load data: %v", err)
			}

			if len(data) != d.EnvSize {
				t.Fatalf("Loaded data size (%x) does not match EnvSize (%x)", len(data), d.EnvSize)
			}

			if !bytes.Equal(data[:32], []byte("\xFD\x95\x64\x9farch=arm\x00baudrate=115200\x00boa")) {
				t.Fatalf("Unexpected data[:32]: %q", data[:32])
			}

			if !bytes.Equal(data[len(data)-16:], []byte("\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")) {
				t.Fatalf("Unexpected data[len(data)-16:]: %q", data[len(data)-16:])
			}
		})
	}

	t.Run("missing", func(t *testing.T) {
		d := ubootenv.Device{
			DeviceName: "testdata/uboot.missing",
			EnvSize:    0x4000,
		}

		if _, err := d.LoadData(); !errors.Is(err, os.ErrNotExist) {
			t.Fatalf("Unexpected error: %v", err)
		}
	})

	// We want to test block devices and MTD but this is hard in pure go.
	t.Run("directory", func(t *testing.T) {
		d := ubootenv.Device{
			DeviceName: "testdata/",
			EnvSize:    0x4000,
		}

		_, err := d.LoadData()
		if err == nil {
			t.Fatalf("Unexpected success to load data from directory")
		}

		if err.Error() != "only regular files are supported" {
			t.Fatalf("Unexpected error: %s", err)
		}
	})

	// Env size mismatch
	t.Run("mismatch", func(t *testing.T) {
		d := ubootenv.Device{
			DeviceName: "testdata/uboot.env",
			EnvSize:    0x5000, // Short of 0x4000
		}

		_, err := d.LoadData()
		if err == nil {
			t.Fatalf("Unexpected success to load data from a file too small to hold it")
		}

		if !errors.Is(err, io.EOF) {
			t.Fatalf("Unexpected error: %s", err)
		}
	})
}

func ubootEnvData(t *testing.T) []byte {
	f, err := os.Open("testdata/uboot.env")
	if err != nil {
		t.Fatal(err)
	}

	defer func() { _ = f.Close() }()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		t.Fatal(err)
	}

	return data
}

// ubootEnvAtOffset returns the name a temporary file with testdata/uboot.env copied at an offset.
func ubootEnvAtOffset(t *testing.T, size, offset int64) (name string) {
	data := ubootEnvData(t)

	name = filepath.Join(t.TempDir(), "uboot.env")

	f, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE, 0o600)
	if err != nil {
		t.Fatal(err)
	}

	defer func() { _ = f.Close() }()

	if err := f.Truncate(size); err != nil {
		t.Fatal(err)
	}

	// Let's figure out where to seek to write the data, so that it ends up
	// at "offset". The value given as input may be positive, to indicate
	// that the data is simply, "at" that offset, or negative, to indicate
	// offset relative to the end of the file.
	whence := io.SeekStart

	if offset < 0 {
		if int64(len(data)) > -offset {
			t.Fatalf("data length is %#x but requested offset is (impossible) %#x", len(data), offset)
		}

		whence = io.SeekEnd
	}

	if _, err := f.Seek(offset, whence); err != nil {
		t.Fatal(err)
	}

	if _, err := f.Write(data); err != nil {
		t.Fatal(err)
	}

	return name
}

func TestDeviceSaveData(t *testing.T) {
	refData := ubootEnvData(t)

	for _, tc := range []struct {
		size, offset int64
	}{
		{0x4000, 0},       // entire file is env data
		{0x8000, 0},       // first half of the file is env data
		{0x8000, -0x4000}, // second half of the file is env data
		{0x8000, 0x4000},  // same as above
		{0x8000, 0x2000},  // env data is in the middle
		{0x8000, -0x6000}, // same as above
	} {
		t.Run(fmt.Sprintf("size:%#x,offset:%#x", tc.size, tc.offset), func(t *testing.T) {
			d := ubootenv.Device{
				DeviceName:   ubootEnvAtOffset(t, tc.size, tc.offset),
				DeviceOffset: int(tc.offset), // TODO(zyga): change the interface to use int64.
				EnvSize:      0x4000,
			}

			if err := d.SaveData(refData); err != nil {
				t.Fatal(err)
			}

			data, err := d.LoadData()
			if err != nil {
				t.Fatal(err)
			}

			if !bytes.Equal(data, refData) {
				t.Fatalf("Loaded data differs from saved data")
			}
		})
	}

	t.Run("missing", func(t *testing.T) {
		d := ubootenv.Device{
			DeviceName: filepath.Join(t.TempDir(), "uboot.missing"),
			EnvSize:    0x4000,
		}

		if err := d.SaveData(refData); err != nil {
			t.Fatal(err)
		}
	})

	// We want to test block devices and MTD but this is hard in pure go.
	t.Run("directory", func(t *testing.T) {
		d := ubootenv.Device{
			DeviceName: "testdata/",
			EnvSize:    0x4000,
		}

		err := d.SaveData(refData)
		if err == nil {
			t.Fatalf("Unexpected success to load data from directory")
		}

		if err.Error() != "open testdata/: is a directory" {
			t.Fatalf("Unexpected error: %s", err)
		}
	})

	// Env size mismatch
	t.Run("mismatch", func(t *testing.T) {
		d := ubootenv.Device{
			DeviceName: "testdata/uboot.env",
			EnvSize:    0x5000, // Short of 0x4000
		}

		err := d.SaveData(refData)
		if err == nil {
			t.Fatalf("Unexpected success to load data from a file too small to hold it")
		}

		if err.Error() != "data size is different from environment size" {
			t.Fatalf("Unexpected error: %s", err)
		}
	})
}

func TestConfigLoadVars(t *testing.T) {
	refData := ubootEnvData(t)

	t.Run("happy", func(t *testing.T) {
		cfg := ubootenv.Config{
			Device: ubootenv.Device{
				DeviceName: "testdata/uboot.env",
				EnvSize:    0x4000,
			},
		}

		vars, err := cfg.LoadVars()
		if err != nil {
			t.Fatal(err)
		}

		if !bytes.Equal(vars.Data, refData) {
			t.Fatalf("Loaded data differs from reference data")
		}

		if vars.HeaderSize != 4 {
			t.Fatal("Non-redundant variables must use 4-byte header")
		}
	})

	t.Run("corrupted", func(t *testing.T) {
		cfg := ubootenv.Config{
			Device: ubootenv.Device{
				DeviceName: ubootEnvCorrupted(t),
				EnvSize:    0x4000,
			},
		}

		_, err := cfg.LoadVars()

		if err == nil {
			t.Fatalf("Unexpected success to load corrupted data")
		}

		if err.Error() != "bad CRC" {
			t.Fatalf("Unexpected error: %s", err)
		}
	})
}

func TestConfigSaveVars(t *testing.T) {
	d := t.TempDir()
	name := filepath.Join(d, "uboot.evn")
	cfg := ubootenv.Config{
		Device: ubootenv.Device{
			DeviceName: name,
			EnvSize:    0x16, // Unreasonably small but possible.
		},
	}
	vars := cfg.NewVars()

	if !bytes.Equal(vars.Data, []byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}) {
		t.Fatalf("unexpected newly created environment: %v", vars.Data)
	}

	if err := cfg.SaveVars(vars); err != nil {
		t.Fatal(err)
	}

	// CRC data is visible.
	if !bytes.Equal(vars.Data, []byte{77, 207, 27, 103, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}) {
		t.Fatalf("unexpected in-memory saved environment: %v", vars.Data)
	}

	data, err := ioutil.ReadFile(name)
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(data, []byte{77, 207, 27, 103, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}) {
		t.Fatalf("unexpected on-disk saved environment: %v", data)
	}
}

// ubootEnvCorrpted returns the name a temporary file with testdata/uboot.env with a fixed corruption.
func ubootEnvCorrupted(t *testing.T) (name string) {
	data := ubootEnvData(t)

	// Flip a pair of bits.
	data[125] |= 0x01
	data[15] ^= 0x01

	name = filepath.Join(t.TempDir(), "uboot.env")

	f, err := os.OpenFile(name, os.O_WRONLY|os.O_CREATE, 0o600)
	if err != nil {
		t.Fatal(err)
	}

	defer func() { _ = f.Close() }()

	if _, err := f.Write(data); err != nil {
		t.Fatal(err)
	}

	return name
}

func TestSmokeSysOTA(t *testing.T) {
	v := ubootenv.Variables{Data: make([]byte, 64)}

	if err := v.Set("SYSOTA_MODE", "normal"); err != nil {
		t.Fatalf("Unexpectedly failed to set SYSOTA_MODE: %v", err)
	}

	if err := v.Set("SYSOTA_ACTIVE", "A"); err != nil {
		t.Fatalf("Unexpectedly failed to set SYSOTA_ACTIVE: %v", err)
	}

	expectedPrefix1 := []byte("SYSOTA_MODE=normal\x00SYSOTA_ACTIVE=A\x00\x00")
	if !bytes.HasPrefix(v.Data, expectedPrefix1) {
		t.Fatalf("Unexpected result: %s", v.Data)
	}

	if err := v.Set("SYSOTA_MODE", "try"); err != nil {
		t.Fatalf("Unexpectedly failed to set SYSOTA_MODE: %v", err)
	}

	expectedPrefix2 := []byte("SYSOTA_MODE=try\x00SYSOTA_ACTIVE=A\x00\x00")
	if !bytes.HasPrefix(v.Data, expectedPrefix2) {
		t.Fatalf("Unexpected result: %s", v.Data)
	}
}

func TestSmoke(t *testing.T) {
	v := ubootenv.Variables{Data: make([]byte, 64)}

	if val, ok := v.Lookup("key"); ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := v.Set("key", "value"); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := v.Lookup("key"); !ok || val != "value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := v.Set("key", "longer value"); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := v.Lookup("key"); !ok || val != "longer value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := v.Set("key", "v"); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := v.Lookup("key"); !ok || val != "v" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if val := v.Get("key"); val != "v" {
		t.Fatalf("Unexpected get result: %v", val)
	}

	if err := v.Set("key", ""); err != nil {
		t.Fatalf("Unexpected failure to set key: %v", err)
	}

	if val, ok := v.Lookup("key"); !ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	v.Delete("key")

	if val, ok := v.Lookup("key"); ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if !bytes.Equal(v.Data, make([]byte, 64)) {
		t.Fatalf("Unexpected difference after modifications: %s", v.Data)
	}
}

func TestSetDuplicateKeyName(t *testing.T) {
	v := ubootenv.Variables{Data: make([]byte, 64)}
	copy(v.Data, "key=one\x00key=two\x00key=three\x00")

	if val, ok := v.Lookup("key"); !ok || val != "one" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	if err := v.Set("key", "value"); err != nil {
		t.Fatalf("Unexpected failure to set value: %v", err)
	}

	if val, ok := v.Lookup("key"); !ok || val != "value" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	expected := make([]byte, 64)
	copy(expected, "key=value\x00key=value\x00key=value\x00")

	if !bytes.Equal(v.Data, expected) {
		t.Fatalf("Unexpected difference after modifications: %s", v.Data)
	}
}

func TestDeleteDuplicateKeyName(t *testing.T) {
	v := ubootenv.Variables{Data: make([]byte, 64)}
	copy(v.Data, "key=one\x00key=two\x00key=three\x00")

	if val, ok := v.Lookup("key"); !ok || val != "one" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}

	v.Delete("key")

	if val, ok := v.Lookup("key"); ok || val != "" {
		t.Fatalf("Unexpected lookup result: %q, %v", val, ok)
	}
}

func TestSetTooLargeValue(t *testing.T) {
	v := ubootenv.Variables{Data: make([]byte, 16)}
	copy(v.Data, "key=value\x00")

	err := v.Set("key", "this is a value too long to fit")
	if !errors.Is(err, ubootenv.ErrInsufficientSpace) {
		t.Fatalf("Unexpected error: %v", err)
	}
}
